import os
import re
import emoji #instalar
import pandas as pd

from string import digits

# Hashtag
def hashtag_csv_to_txt(name,df):
    with open(name,"w",encoding='utf-8') as export_txt:
        counter = 0
        for i in range(len(df)):
            if(type(df[df.columns[3]][i]) == float): # Caso o post não tenha hashtag, deixamos a linha vazia
                export_txt.writelines('\n')
                counter = counter + 1
            else:
                line_x = df[df.columns[3]][i].split() # Separar
                line_x.append('\n')                   # Quebra de linha
                export_txt.writelines(" ".join(line_x))
    return counter

# Legendas
def remove_hashtags(str_l):
    str_l = ' '.join([ word for word in str_l.split() if not word.startswith('#') ])
    return str_l

def remove_emoji(str_l):
    return emoji.get_emoji_regexp().sub(u'',str_l)

def https_splited_finder(str_l):
    test = str_l.find('https://t. ')
    if (test == -1):
        return False
    else:
        return True

def fix_https(str_l):
    while(https_splited_finder(str_l) == True):
        if('https' in str_l):
            https_s = str_l.find('https://t. ')
            https_s_l = https_s+len('https://t.')
            str_l = str_l[:https_s_l] + '' + str_l[https_s_l+1:]
    return str_l

def remove_links_and_dots(str_l):
    str_l = ' '.join([word for word in str_l.split() if not word.startswith('https') if not word=='.'])  
    return str_l

def remove_non_latin(str_ln):
    return re.compile('[^\u0020-\u024F]').sub('',str_ln)

def remove_symbols(str_ln):
    return re.sub(r'[^\w]', ' ',str_ln)

def remove_underline(str_ln):
    return str_ln.replace('_', '')

def remove_number(str_ln):
    return str_ln.translate(str.maketrans('', '', digits))

def organize_line(str_ln):
    return ' '.join(str_ln.split())

def legendas_csv_to_txt(name,df):
    with open(name,"w",encoding='utf-8') as export_txt:
        counter = 0
        for i in range(len(df)):
            linha = organize_line(remove_number(remove_underline(remove_symbols(remove_non_latin(remove_links_and_dots(fix_https(remove_emoji(remove_hashtags(df[df.columns[2]][i])))))))))       
            if(linha == ''): # Caso o post não tenha legenda válida, deixamos a linha vazia
                export_txt.writelines('\n')
                counter = counter + 1
            else:
                export_txt.writelines(linha+' \n')
    return counter

# Exportar tudo
arqs = [i for i in os.listdir() if i[-3:] =='csv']
print(arqs)

save_txt = False
if (save_txt ==True):
    nulls_counter_h = []
    nulls_counter_l = []
    for i in arqs:
        if(i=='Twitter.csv'):
            df_s = pd.read_csv(i)
        else:
            df_s = pd.read_csv(i,header=None)
        name_h = "hashtags_"+i[:-4]+".txt"
        nulls_counter_h.append(hashtag_csv_to_txt(name_h,df_s))
        name_l = "legendas_"+i[:-4]+".txt"
        nulls_counter_l.append(legendas_csv_to_txt(name_l,df_s))
        print(name_h,'e',name_l,' gerados.')

    