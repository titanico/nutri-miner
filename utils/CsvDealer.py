import csv
import json
import pandas as pd
from datetime import date
from os import chdir
from glob import glob

class CsvDealer():

    def __init__(self):
        self.num = 1

    def _produceOneCSV(self,files_list, output_name, index : bool = False):
        '''
            Junta os csv em um unico csv
        '''
        result_obj = pd.concat([pd.read_csv(file) for file in files_list])
        result_obj.to_csv(output_name, index = index, encoding="utf-8")

    def merge_csv(self,dir,output_name,index : bool = False):
        '''
            concatena todos os csv presentes em dir e junta em um unico
            csv denominado output_name
        '''
        chdir(dir) #muda pro diretório do csv a ser lido
        extension = "csv"
        files_list = [file for file in glob('*.{}'.format(extension))]
        print(files_list)

        self._produceOneCSV(files_list, output_name ,index)

    def remove_duplicates(self,dir,output,keys_list, keep: str = 'first'):
        '''
            remove do csv em dir todas as colunas que estejam duplicadas
            comparando as colunas que estejam em keys_list
            (por padrão matem-se a primeira ocorrencia de repetição)
            o novo csv é salvo com o nome output
        '''
        raw = pd.read_csv(dir, keep_default_na = False) #arquivo 'cru'
        raw.drop_duplicates(subset = keys_list, inplace = True, keep = keep)#remove duplicatas
        raw.to_csv(output, index = True)#salva em um csv de nome output

    def write_results(self, posts, tipo):
        
        dir = '' #coloque aqui o diretório para salvar os arquivos

        result_file = open(dir + tipo + '.csv', mode='a', encoding='utf-8')
        writer = csv.DictWriter(result_file, fieldnames=['Número', 'Hashtag de Busca', 'Legenda da Postagem','Hashtags do Post', 'URL', 'Data', 'Username','Followers', 'Like Count', 'Comment Count' ], quotechar='"', quoting=csv.QUOTE_ALL)

        if self.num == 1:
            writer.writeheader()

        for post in posts:

            aux_result = dict()

            aux_result['Número'] = self.num
            self.num+=1

            aux_result['Hashtag de Busca'] = post[0]

            aux_result['Legenda da Postagem'] = post[1]
            aux_result['Hashtags do Post'] = post[2]
            aux_result['URL'] = post[3]
            aux_result['Data'] = post[4]
            aux_result['Username'] = post[5]
            aux_result['Followers'] = post[6]
            #aux_result['Retweet Count'] = str(tweet['retweet_count'])
            #aux_result['Favorite Count'] = str(tweet['favorite_count'])
            aux_result['Like Count'] = post[7]
            aux_result['Comment Count'] = post[8]

            hashtags_do_post = ''

            for hashtag in post[2]:
                if hashtag.endswith('\n'):
                    hashtags_do_post = hashtags_do_post
                else:
                    hashtags_do_post = hashtags_do_post + hashtag + '\n'

            aux_result['Hashtags do Post'] = hashtags_do_post

            writer.writerow(aux_result)

        result_file.close()

