from datetime import date as dt
from datetime import timedelta
from utils.CsvDealer import CsvDealer

def concatena_csv(tipo, dir):
    
    today = dt.today()
    date_today = str(today.day) + '-' + str(today.month)
    
    week_ago = today - timedelta(days=6)
    date_week_ago = str(week_ago.day) + '-' + str(week_ago.month)
 
    aux_name = tipo + '_' + date_week_ago + '_' + date_today

    output_duplicated = "{}_raw.csv".format(aux_name)
    output_csv = aux_name + '.csv'

    csvhandler = CsvDealer()
    fieldnames=['Legenda da Postagem', 'Username'] #Colunas a serem comparadas para remover duplicatas
    csvhandler.merge_csv(dir, output_duplicated)
    csvhandler.remove_duplicates("{}/{}".format(dir,output_duplicated),output_csv,fieldnames)

    return aux_name


