import datetime as dt

dict_meses = {
    'janeiro': '01',
    'fevereiro': '02',
    'março': '03',
    'abril': '04',
    'maio': '05',
    'junho': '06',
    'julho': '07',
    'agosto': '08',
    'setembro': '09',       
    'outubro': '10',     
    'novembro': '11',
    'dezembro': '12',          
}

def convert_date(date):

    try:

        t = dt.datetime.now()

        date = date.split(' às')[0]

        split_date = date.split(' de')

        if (len(split_date) == 3):
            day = str(split_date[0])
            month = dict_meses[split_date[1][1:]]
            year = str(split_date[2])[1:]
        elif(len(split_date) == 2):
            day = str(split_date[0])
            month = dict_meses[split_date[1][1:]]
            year = str(t.year)
        elif(len(split_date) == 1):
            day = str(split_date[0])
            month = str(t.month)
            year = str(t.year)

        if (len(day)==1):
            day = '0' + day

        date = day + '/' + month + '/' + year

    except:

        date = ''

    return date
