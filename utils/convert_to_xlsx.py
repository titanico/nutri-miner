from openpyxl import Workbook
import csv

def convert_to_xlsx(dir, file_name):
    wb = Workbook()
    ws = wb.active
    file_name = dir + '/' + file_name
    with open(file_name + '.csv', 'r') as f:
        for row in csv.reader(f):
            ws.append(row)
    wb.save(file_name + '.xlsx')