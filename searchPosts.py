from AllTwitterApplication import all_twitter
from FacebookApplication import facebook
from LockdownFacebookApplication import lockdown_facebook
from CovidFacebookApplication import covid_facebook
import threading

thread_facebook = threading.Thread(target=facebook)
thread_lockdown_facebook = threading.Thread(target=lockdown_facebook)
thread_covid_facebook = threading.Thread(target=covid_facebook)
thread_twitter = threading.Thread(target=all_twitter)

thread_twitter.start()
thread_facebook.start()
thread_lockdown_facebook.start()
thread_covid_facebook.start()

