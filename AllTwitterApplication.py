from twython import Twython
from utils.CsvDealer import CsvDealer
import time

consumer_key = 'ZZ0iVCYL7et3u4CVw5P8gboeg' #Coloque sua API key
consumer_secret_key = 'Mm8ZUdAICJqef0BnGYdA72ZbZWbDqA18kHtbHJctxOYsAs0WzN' #Coloque sua API Secret key

class Miner():

    def __init__(self, consumer_key, consumer_secret_key):

        self.twitter = Twython(consumer_key, consumer_secret_key)
        self.tweets = []

    def search_tweets(self, query, counter):

        results = self.twitter.search(q=query, count=counter)
        new_tweets = results['statuses']

        for tweet in new_tweets:

            if (tweet['text'][:3] != 'RT ' and tweet not in self.tweets):
                self.tweets.append(tweet)

    def get_posts_details(self, hashtag_busca):

        posts = []
        for tweet in self.tweets:
            try:
                usuario = tweet['user']['screen_name']
            except:
                usuario = ''
            try:
                url = tweet['entities']['urls'][0]['expanded_url']
            except:
                url = ''

            try:
                post_date_time = tweet['created_at']
            except:
                post_date_time = ""

            comment_count = ''

            try:
                like_count = tweet['favorite_count']
                like_count = str(like_count)
            except:
                like_count = ''

            try:
                friends = tweet['user']['followers_count']
            except:
                friends = ''

            hashtags_post = []

            try:
                for hashtag in tweet['entities']['hashtags']:
                    hashtags_post.append('#' + hashtag['text'])
            except:
                pass

            searched_hashtag = hashtag_busca

            try:
                split_post = tweet['text'].split('.')

                tweet['text'] = split_post[0]

                for item in split_post[1:]:
                    tweet['text'] = tweet['text']+ '.\n' + item

            except:
                pass

            try:

                split_post = tweet['text'].split('!')

                tweet['text'] = split_post[0]

                for item in split_post[1:]:
                    tweet['text'] = tweet['text']+ '!\n' + item

            except:
                pass

            try:

                split_post = tweet['text'].split('?')

                tweet['text'] = split_post[0]

                for item in split_post[1:]:
                    tweet['text'] = tweet['text']+ '?\n' + item

            except:
                pass

            try:

                split_post = tweet['text'].split(';')

                tweet['text'] = split_post[0]

                for item in split_post[1:]:
                    tweet['text'] = tweet['text']+ ';\n' + item

            except:
                pass

            try:

                split_post = tweet['text'].split('#')

                tweet['text'] = split_post[0]

                for item in split_post[1:]:
                    tweet['text'] = tweet['text'] + '\n#' +item

            except:
                pass

            post = []
            post.append(searched_hashtag)
            post.append(tweet['text'])
            post.append(hashtags_post)
            post.append(url)
            post.append(post_date_time)
            post.append(usuario)
            post.append(friends)
            post.append(like_count)
            post.append(comment_count)



            posts.append(post)
        return posts

def all_twitter():

        miner = Miner(consumer_key, consumer_secret_key)

        csv_dealer = CsvDealer()

        hashtags = ['#alimentacaosaudavel', '#coronavid19', '#covid19', '#coronavírus', '#coronavirus', '#covid_19', '#fiqueemcasa', '#quarentena', '#quarentenanacozinha', '#isolamentosocial']

        all_posts = []

        contador = 0

        while True:

            try:

                for hashtag in hashtags:

                    posts_to_write = []

                    miner.search_tweets(hashtag, 250)
                    posts = miner.get_posts_details(hashtag)
                    for post in posts:
                        if post not in all_posts:
                            posts_to_write.append(post)
                    try:
                        if hashtag == '#alimentacaosaudavel':
                            csv_dealer.write_results(posts_to_write, 'Twitter')
                        elif hashtag in ['#coronavid19', '#covid19', '#coronavírus', '#coronavirus', '#covid_19']:
                            csv_dealer.write_results(posts_to_write, 'TwitterCovid')
                        else:
                            csv_dealer.write_results(posts_to_write, 'TwitterLockdown')
                    except:
                        print('Nenhum post novo foi encontrado')
                    miner.tweets.clear()

                    print('Consulta realizada para a hashtag {} no Twitter'.format(hashtag))
                    time.sleep(60)

            except:

                print('Houve um erro na pesquisa da hashtag: {}'.format(hashtag))
                contador += 1
                if contador >= 3:
                    time.sleep(300)
                    contador = 0