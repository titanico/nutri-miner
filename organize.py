from utils.concatena_csv import concatena_csv as concat
from utils.convert_to_xlsx import convert_to_xlsx as convert

types = {
    '1':'Twitter',
    '2':'Covid_Twitter',
    '3':'Lockdown_Twitter',
    '4':'Facebook',
    '5':'Covid_Facebook',
    '6':'Lockdown_Facebook'
}

default_dir = '' #Coloque aqui o diretório padrão para organizar

text_types = '\nSr. John, escolha uma opção:\n\n1- {}\n2- {}\n3- {}\n4- {}\n5- {}\n6- {}\n\n'.format(types['1'], types['2'], types['3'], types['4'], types['5'], types['6'])

results_type = input(text_types)

try:
    file_name = concat(types[results_type], dir=default_dir)
except:
    print('\nNão foi possível concatenar os csvs, verifique se a pasta escolhida contém arquivos')

try: 
    convert(default_dir, file_name)
except:
    pass
