from selenium import webdriver
from utils.CsvDealer import CsvDealer
import time
from utils.facebook_date import convert_date

def login(driver, username, password):
    driver.get('https://wwww.facebook.com/')
    driver.find_element_by_id('email').send_keys(username)
    driver.find_element_by_id('pass').send_keys(password)
    driver.find_element_by_id('loginbutton').click()

def seemore(driver, hashtag):

    try:

        time.sleep(10)

        driver.get('https://www.facebook.com/search/posts/?q=%23' + hashtag.replace('#', ''))

        time.sleep(10)

        see_more = driver.find_element_by_xpath("//*[contains(text(), 'Ver todas as publicações públicas para')]")
        see_more_link = see_more.find_element_by_xpath("../a").get_attribute('href')

        driver.get(see_more_link)

    except:
        print("Não foi possível realizar a consulta da hashtag {} para o Facebook".format(hashtag))

def get_post_links(driver):

    try:
        for i in range(10):
            driver.execute_script('window.scroll(0, 20000)')
            time.sleep(2+i)
    except:
        pass

    time.sleep(10)

    links = [a.get_attribute('href') for a in driver.find_elements_by_tag_name('a')]

    post_links = []

    for link in links:
        try:
            if link.split('/')[4] == 'posts':
                if link.split('/')[3] != 'search':
                    if link not in post_links:
                        post_links.append(link)
        except:
            pass

    return post_links

def get_posts_details(driver, post_links, hashtag):

    posts = []

    for post_link in post_links:
                
        time.sleep(60)
        driver.execute_script('window.scroll(0, 10)')
        driver.execute_script('window.scroll(0, -10)')

        try:

            driver.get(post_link)

            try:
                postagem = driver.find_element_by_tag_name('p').text 
                paragraphs = driver.find_elements_by_tag_name('p')
                last_local_y = driver.find_element_by_tag_name('p').location['y']
                for p in paragraphs[1:]:
                    local_y = p.location['y']
                    if (local_y - last_local_y) < 600: 
                        postagem = postagem + '\n' + p.text
                        last_local_y = local_y
                    else:
                        break
            except: 
                postagem = 'Imagem'
    
            url = post_link

            try:
                post_date_time = driver.find_element_by_class_name('timestampContent').text
            except:
                post_date_time = ''

            post_date_time = convert_date(post_date_time)

            usuario = post_link.split('/')[3]

            try:
                like_count = int(driver.find_elements_by_tag_name('form')[1].text.split("\n")[0])
                like_count = str(like_count)
            except:
                like_count = ''

            for item in driver.find_elements_by_tag_name('form')[1].text.split("\n"):

                if ' comentário' in item:
                    try:
                        comment_count_test = int(item.replace(' comentário', '').replace('s',''))
                        comment_count = comment_count_test
                        break
                    except:
                        pass
                else:
                    comment_count = '0'
                #friends = "Placeholder"
            driver.get("https://www.facebook.com/"+usuario)

            time.sleep(5)

            try:
                for item in driver.find_element_by_id('PagesProfileHomeSecondaryColumnPagelet').text.split('\n'):
                    if ' pessoas estão seguindo isso' in item or ' pessoa está seguindo isso' in item: 
                        friends_test = int(item.replace(' pessoas estão seguindo isso','').replace(' pessoa está seguindo isso', '').replace('.', ''))
                        friends = friends_test
                        break
            except:
                try:
                    driver.get("https://www.facebook.com/"+usuario+"/members")
                    for item in driver.find_element_by_id('groupsMemberBrowser').text.split('\n'):
                        if "Membros" in item or "Membro" in item:
                            friends_test = int(item.replace('Membros', '').replace('Membro', '').replace('.', ''))
                            friends = friends_test
                            break
                except:
                    friends = ''

            hashtags = []

            if not postagem.startswith('#'):

                try:
                    raw_hashtags = postagem.split('#')

                    for item in raw_hashtags[1:]:
                        hashtags.append('#' + item.split(' ')[0])

                except:
                    pass

            else:

                try:
                    raw_hashtags = postagem.split('#')

                    for item in raw_hashtags:
                        hashtags.append('#' + item.split(' ')[0])

                except:
                    pass

            searched_hashtag = hashtag

            try:

                split_post = postagem.split('.')

                postagem = split_post[0]

                for item in split_post[1:]:
                    postagem = postagem+ '.\n' + item

            except:
                pass

            try:

                split_post = postagem.split('!')

                postagem = split_post[0]

                for item in split_post[1:]:
                    postagem = postagem+ '!\n' + item

            except:
                pass

            try:

                split_post = postagem.split('?')

                postagem = split_post[0]

                for item in split_post[1:]:
                    postagem = postagem+ '?\n' + item

            except:
                pass

            try:

                split_post = postagem.split(';')

                postagem = split_post[0]

                for item in split_post[1:]:
                    postagem = postagem+ ';\n' + item

            except:
                pass

            try:

                split_post = postagem.split('#')

                postagem = split_post[0]

                for item in split_post[1:]:
                    postagem = postagem + '\n#' +item

            except:
                pass


            post = []
            post.append(searched_hashtag)
            post.append(postagem)
            post.append(hashtags)
            post.append(url)
            post.append(post_date_time)
            post.append(usuario)
            post.append(friends)
            post.append(like_count)
            post.append(comment_count)


            posts.append(post)

        except:
            pass

    return posts

def lockdown_facebook():

    username = '' #Coloque seu e-mail
    password = '' #Coloque sua senha
    driverpath = '' #coloque o diretorio do seu chromedriver

    hashtags = ['#fiqueemcasa', '#quarentena', '#quarentenanacozinha', '#isolamentosocial']

    csv_dealer = CsvDealer()

    driver = webdriver.Chrome(driverpath)

    login(driver, username, password)

    all_posts = []

    while True:

        for hashtag in hashtags:

            seemore(driver, hashtag)
            post_links = get_post_links(driver)
            posts_to_test = get_posts_details(driver, post_links, hashtag)
            posts_to_write = []

            for post in posts_to_test:
                if post not in all_posts:
                    posts_to_write.append(post)
                    all_posts.append(post)
            try:        
                csv_dealer.write_results(posts_to_write, 'Facebook_Covid')
            except:
                print("Não existem mais posts disponíveis para " + hashtag)

            time.sleep(900)

if __name__ == '__main__':
    lockdown_facebook()